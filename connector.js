Connector =
{
    FormData:
    {
        Name: { Type: "text", Title: "Name", Value: "Jenkins" },
        ServerUrl: { Type: "text", Title: "Jenkins Server URL", Required: true, Restart: true, ProtocolCheck: true },
        Username: { Type: "text", Title: "Username", Required: true, Restart: true },
        Token: { Type: "text", Title: "Token", Required: true, Restart: true },
        ImportantRefs: { Type: "regex", Title: "Important Branches (Regex)", Value: "(master|develop|release/(.*))" },
        Interval: { Type: "number", Title: "Request Interval (in milliseconds)", Value: 1000, Required: true }
    },

    Shared:
    {
        StatusMap:
        {
            "SUCCESS": "Success",
            "FAILURE": "Failed",
            "ABORTED": "Canceled"
        }
    },

    Index: 0,

    _refreshTimer: null,
    _refreshing: false,

    _responseProcessors: null,
    _projects: null,
    _serverUrl: null,

    _crumbKey: null,
    _crumbValue: null,

    Logger: null,
    Requestor: null,
    DataSink: null,
    ConnectorConfig: null,

    Initialize: function()
    {
        this._projects = {};
        this._responseProcessors = {};
        this._responseProcessors.projects = this.ParseProjects;
        this._responseProcessors.crumb = this.ParseCrumb;
    },

    Start: function()
    {
        if (this._refreshTimer != null)
        {
            this.Logger.Log("Already started.", this.Logger.LogLevels.Info);
            return;
        }

        this._serverUrl = this.ConnectorConfig.ServerUrl + (this.ConnectorConfig.ServerUrl && this.ConnectorConfig.ServerUrl.endsWith("/") ? "" : "/");

        this._refreshTimer = setInterval(this.Refresh.bind(this), this.ConnectorConfig.Interval);
        this.Refresh();
    },

    Stop: function()
    {
        if (this._refreshTimer == null)
        {
            return;
        }

        clearInterval(this._refreshTimer);
        this._refreshTimer = null;
    },

    Refresh: function()
    {
        if (this._refreshing)
        {
            this.Logger.Log("Already refreshing, skipping...", this.Logger.LogLevels.Info, this, this);
            return;
        }

        if (!this._crumbKey)
        {
            this.RequestInformation("crumb", 0, "GET", this._serverUrl + "crumbIssuer/api/json");
        }
        else
        {
            this.RequestInformation("projects", 0, "POST", this._serverUrl + "api/json?tree=jobs[displayName,url,result,lastBuild[displayName,result,timestamp,estimatedDuration],jobs[displayName,url,result,lastBuild[displayName,result,timestamp,estimatedDuration],jobs[displayName,url,result,lastBuild[displayName,result,timestamp,estimatedDuration],jobs[displayName,url,result,lastBuild[displayName,result,timestamp,estimatedDuration],jobs]]]]&depth=4");
        }
    },

    RequestInformation: function(requestType, identifier, method, url, callbackDone)
    {
        this._refreshing = true;

        this.Logger.Log("Requesting " + requestType + " (" + identifier + ")...", this.Logger.LogLevels.Debug, this);

        var options = {
            BasicAuth: { User: this.ConnectorConfig.Username, Password: this.ConnectorConfig.Token },
            Headers: { }
        };

        if (this._crumbKey)
        {
            options.Headers[this._crumbKey] = this._crumbValue;
        }

        var methods = {
            GET: this.Requestor.Get.bind(this.Requestor),
            POST: this.Requestor.Post.bind(this.Requestor)
        };

        var self = this;
        methods[method](url,
            function(data)
            {
                self._refreshing = false;
                App.ShowWarning(false);

                var processor = self._responseProcessors[requestType];
                if (processor)
                {
                    processor.call(self, identifier, data);
                }
                else
                {
                    self.Logger.Log("No response processor for " + requestType + " found.", self.Logger.LogLevels.Error, self);
                }

                if (callbackDone)
                {
                    callbackDone(data);
                }
            },
            function(jqXhr, textStatus, errorThrown)
            {
                self.RequestFailed.call(self, identifier);

                // TODO: Localize
                var message = errorThrown || "Check Server URL or press F12 to check the console.";
                App.ShowWarning(true, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ": " + message);
                self.Logger.Log("Error while sending web request", self.Logger.LogLevels.Error, self);
            },
            function()
            {
                self._refreshing = false;
            },
            options
        );
    },

    ParseCrumb: function(identifier, data)
    {
        this.Logger.Log("Parsing crumb...", this.Logger.LogLevels.Debug, this);

        if (!data.crumb || !data.crumbRequestField)
        {
            App.ShowWarning(true, "Invalid Crumb response from Jenkins, please check your Jenkins instance.");
            return;
        }

        this._crumbKey = data.crumbRequestField;
        this._crumbValue = data.crumb;
    },

    ParseProjects: function(identifier, data, preventUpdate)
    {
        this.Logger.Log("Parsing project list for " + identifier + "...", this.Logger.LogLevels.Debug, this);

        var projects = this._projects;
        for (var key in data.jobs)
        {
            var element = data.jobs[key];
            if (!element.url)
            {
                // Reached max depth
                continue;
            }

            var className = element._class.toLowerCase();

            var relativeUrl = Utils.ParseUrl(element.url).Path;
            var id = this.DataSink.GetValidIdentifier(relativeUrl);
            var project = this._projects[id] ? this._projects[id] : { Status: "Unknown", Username: "Unknown" };

            if (element.lastBuild)
            {
                this.ParseBuild(project, element.lastBuild);
                project.Reference = element.lastBuild.displayName;
            }
            else
            {
                project.Status = "NoBuilds";
            }

            if (element.jobs && element.jobs.length)
            {
                // It is a folder
                if (className.endsWith("multibranchproject"))
                {
                    this.ParseBranches(project, element.jobs);
                }
                else
                {
                    this.ParseProjects(identifier, element, true);
                    continue;
                }
            }

            projects[id] = project;
            project.Identifier = id;
            project.Url = element.url;
            project.RelativeUrl = relativeUrl;
            project.Name = element.displayName;
        }

        if (!preventUpdate)
        {
            this.DataSink.Update(this._projects, null, this);
        }
    },

    ParseBuild: function(project, data)
    {
        project.Status = (data.result ? this.Shared.StatusMap[data.result] : "Running") || "Unknown";
        project.ImportantRefStatus = project.Status;

        if (project.Status === "Running")
        {
            project.StartedAt = data.timestamp ? new Date(data.timestamp).toISOString() : null;
            project.EstimatedEnd = data.timestamp && data.estimatedDuration > 0 ? new Date(data.timestamp + data.estimatedDuration).toISOString() : null;
        }
        else
        {
            project.FinishedAt = data.timestamp ? new Date(data.timestamp + (data.duration || 0)).toISOString() : null;
        }
    },

    ParseBranches: function(project, jobs)
    {
        var branches = jobs.filter(function(job) { return job.lastBuild; });
        branches.sort(function(a, b) { return a.lastBuild.timestamp > b.lastBuild.timestamp ? 1 : -1; });

        // Find branch to display and check important references
        var branch = null;
        var importantRefsFailed = false;
        for (var index in branches)
        {
            if (branches[index].displayName.match(this.ConnectorConfig.ImportantRefs))
            {
                importantRefsFailed = importantRefsFailed || branches[index].lastBuild.result === "FAILURE";
            }

            if (!branches[index].lastBuild.result)
            {
                // Job is running
                branch = branches[index];
            }
        }
        branch = branch || branches[0];

        this.ParseBuild(project, branch.lastBuild);
        project.Reference = branch.displayName;
        project.ImportantRefStatus = importantRefsFailed ? "Failed" : "Success";
    },

    RequestFailed: function(identifier)
    {
        this._refreshing = false;
        this.Logger.Log(identifier);
    }
};